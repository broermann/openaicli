# OpenAI API tests


##### Get  API key 

Generate one on  https://platform.openai.com/account/api-keys and put it in .openaikey


##### Set var

    OPENAI_API_KEY=$(<~/.openaikey)


##### Test command 

    curl https://api.openai.com/v1/models \
      -H "Authorization: Bearer $OPENAI_API_KEY" \
      -H "OpenAI-Organization: org-wBe5NTlU5VUjhv9idQWdgRzX"


##### Request

    curl https://api.openai.com/v1/chat/completions \
      -H "Content-Type: application/json" \
      -H "Authorization: Bearer $OPENAI_API_KEY" \
      -d '{
         "model": "gpt-3.5-turbo",
         "messages": [{"role": "user", "content": "Say this is a test!"}],
         "temperature": 0.7
       }'

##### List models

    curl https://api.openai.com/v1/models \
    -H "Authorization: Bearer $OPENAI_API_KEY" \
    tee models.list

    cat models.list |jq -r '.data[]|.id'| sort


Refs:
(https://platform.openai.com/docs/api-reference/introduction)

