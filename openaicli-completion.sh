#!/bin/bash
# Bernd Broermann


OPENAI_URL="api.openai.com"
OPENAI_API_KEY=$(<~/.openaikey)


MODEL="gpt-3.5-turbo"
ROLE="user"
TEMPERATURE=0.7

TIMESTAMP=$(date "+%Y%m%dT%H%M%S")
LOGDIR=logs/$TIMESTAMP
REPORT=$LOGDIR/report.md

[ -d $LOGDIR ] || mkdir -p $LOGDIR

export DEBUG=false
export VERBOSE=false

GETOPT=$(getopt -o     'vd' \
    --long 'verbose,debug,model:,role:,temperature:' \
    -n "${0##*/}" -- "$@")
RC=$?;   if [ $RC -ne 0 ]; then echo 'getopt terminating...' >&2; exit 1; fi
eval set -- "$GETOPT"; unset GETOPT
while true; do
    case "$1" in
        '-v'|'--verbose') VERBOSE=true; shift; continue ;;
        '-d'|'--debug') DEBUG=true; shift; continue ;;
        '-m'|'--model') MODEL=$2; shift 2; continue ;;
        '-r'|'--role') ROLE=$2; shift 2; continue ;;
        '-t'|'--temperature:') TEMPERATURE=$2; shift 2; continue ;;
        '--') shift; break ;;
        *) echo 'getopt internal error!' >&2; exit 1 ;;
    esac
done

CONTENT="$*"

REQUEST=$(jq -n --arg model $MODEL --arg role $ROLE --arg content "$CONTENT" --argjson temperature $TEMPERATURE '{
        "model": $model,
        "messages": [
            { "role": "user", "content": $content }
            ],
        "temperature": $temperature
    }'
)

echo $REQUEST | jq '.' > $LOGDIR/request.json

OPENAI_API_ENDPOINT="/v1/chat/completions"

RESPONSE=$(curl -s https://${OPENAI_URL}${OPENAI_API_ENDPOINT} \
      -H "Content-Type: application/json" \
      -H "Authorization: Bearer $OPENAI_API_KEY" \
      -d "$REQUEST")

echo "$RESPONSE" | jq '.' >  $LOGDIR/response.json

RESPONSE_ROLE=$(echo "$RESPONSE" | jq -r '.choices[].message.role')
RESPONSE_CONTENT=$(echo "$RESPONSE" | jq -r '.choices[].message.content' )

cat > $REPORT << EOF
### https://${OPENAI_URL}${OPENAI_API_ENDPOINT}

Date: $TIMESTAMP
Model: $MODEL

**${ROLE}:**
$CONTENT
EOF

echo "**${RESPONSE_ROLE}:**" | tee -a $REPORT
echo "$RESPONSE_CONTENT" | tee -a $REPORT

