#!/bin/bash



OPENAI_URL="api.openai.com"
OPENAI_API_KEY=$(<~/.openaikey)
OPENAI_API_ENDPOINT="/v1/images/edits"


PROMPT="$*"
N=3
SIZE="1024x1024"

TIMESTAMP=$(date "+%Y%m%dT%H%M%S")
LOGDIR=logs/$TIMESTAMP

[ -d $LOGDIR ] || mkdir -p $LOGDIR



REQUEST=$(jq -n --arg prompt "$PROMPT" --argjson n $N  --arg size $SIZE ' {
  "prompt": $prompt,
  "n": $n,
  "size": $size
}
'
)




echo $REQUEST | jq '.' > $LOGDIR/request.json


RESPONSE=$(curl -s https://${OPENAI_URL}${OPENAI_API_ENDPOINT} \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $OPENAI_API_KEY" \
  -d "$REQUEST"
)

echo "$RESPONSE" | jq '.' | tee   $LOGDIR/response.json
