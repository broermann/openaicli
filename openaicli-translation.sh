#!/bin/bash
# Bernd Broermann


OPENAI_URL="api.openai.com"
OPENAI_API_KEY=$(<~/.openaikey)
OPENAI_API_ENDPOINT="/v1/audio/translations"

MODEL="whisper-1"
FILE="$1"

TIMESTAMP=$(date "+%Y%m%dT%H%M%S")
LOGDIR=logs/$TIMESTAMP

[ -d $LOGDIR ] || mkdir -p $LOGDIR





REQUEST=$(jq -n --arg model $MODEL --arg file $FILE ' {
        "file": $file,
        "model": $model
        }
'
)

echo $REQUEST | jq '.' > $LOGDIR/request.json

RESPONSE=$(
curl -s https://${OPENAI_URL}${OPENAI_API_ENDPOINT} \
  -H "Authorization: Bearer $OPENAI_API_KEY" \
  -H "Content-Type: multipart/form-data" \
  -F file="@$FILE" \
  -F model="$MODEL"
)

echo "$RESPONSE" | jq '.' | tee  $LOGDIR/response.json


